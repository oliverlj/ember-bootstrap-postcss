'use strict';

const componentDependencies = {
  'bs-button-group': ['bs-button'],
  'bs-accordion': ['bs-collapse'],
  'bs-dropdown': ['bs-button', 'bs-link-to'],
  'bs-form': ['bs-button'],
  'bs-modal-simple': ['bs-modal'],
  'bs-modal': ['bs-button'],
  'bs-nav': ['bs-link-to', 'bs-dropdown'],
  'bs-navbar': ['bs-nav', 'bs-button', 'bs-link-to'],
  'bs-popover': ['bs-contextual-help'],
  'bs-tab': ['bs-nav'],
  'bs-tooltip': ['bs-contextual-help'],
};

const emberBootstrapContent = function (allowlistParam) {
  return allowlistParam
    ? generateAllowlist(allowlistParam).flatMap((e) => purgecssfiles(e))
    : allpurgecssfiles;
};

const allpurgecssfiles = [
  './node_modules/ember-bootstrap/addon/components/**.hbs',
  './node_modules/ember-bootstrap/addon/components/**.js',
  './node_modules/ember-bootstrap-postcss/purgecss/**.js',
];

const purgecssfiles = function (component) {
  return [
    `./node_modules/ember-bootstrap/addon/components/${component}.hbs`,
    `./node_modules/ember-bootstrap/addon/components/${component}.js`,
    `./node_modules/ember-bootstrap/addon/components/${component}/**.hbs`,
    `./node_modules/ember-bootstrap/addon/components/${component}/**.js`,
    `./node_modules/ember-bootstrap-postcss/purgecss/${component}.js`,
  ];
};

const generateAllowlist = function (allowlist) {
  const list = [];

  if (!allowlist) {
    return list;
  }

  function _addToAllowlist(item) {
    if (list.indexOf(item) === -1) {
      list.push(item);

      if (componentDependencies[item]) {
        componentDependencies[item].forEach(_addToAllowlist);
      }
    }
  }

  allowlist.forEach(_addToAllowlist);
  return list;
};

// eslint-disable-next-line no-undef
module.exports = {
  emberBootstrapContent,
};
