# ember-bootstrap-postcss

An addon to configure ember bootstrap with postcss. Enable the use of purgecss.

## Compatibility

* Ember.js v3.20 or above
* Ember CLI v3.20 or above
* Node.js v12 or above

## Installation

```sh
yarn add -D @csstools/postcss-sass ember-cli-postcss postcss-scss
```

if purgecss is wanted, install this additional librairies

```sh
yarn add -D ember-bootstrap-postcss @fullhuman/postcss-purgecss
```

## Usage

### Post css configuration

```js
// ember-cli-build.js
'use strict';

const EmberAddon = require('ember-cli/lib/broccoli/ember-addon');

module.exports = function (defaults) {
  const app = new EmberAddon(defaults, {
    'ember-bootstrap': {
      importBootstrapFont: false,
      bootstrapVersion: 4,
      importBootstrapCSS: false,
    },
    postcssOptions: {
      compile: {
        enabled: true,
        extension: 'scss',
        parser: require('postcss-scss'),
        plugins: [
          {
            module: require('@csstools/postcss-sass'),
            options: {
              includePaths: ['node_modules/bootstrap/scss'],
            },
          },
        ],
      },
    },
  });

  ...

};
```

```scss
// app/styles/app.scss
@import 'bootstrap';
```

### Purge css configuration

```js
// ember-cli-build.js
'use strict';

const EmberAddon = require('ember-cli/lib/broccoli/ember-addon');
const { emberBootstrapContent } = require('ember-bootstrap-postcss/purgecss');

const emberBoostrap = {
  importBootstrapFont: false,
  bootstrapVersion: 4,
  importBootstrapCSS: false,
  whitelist: ['bs-collapse', 'bs-navbar'],
};

module.exports = function (defaults) {
  const app = new EmberAddon(defaults, {
    'ember-bootstrap': emberBoostrap,
    postcssOptions: {
      compile: {
        enabled: true,
        extension: 'scss',
        parser: require('postcss-scss'),
        plugins: [
          {
            module: require('@csstools/postcss-sass'),
            options: {
              includePaths: ['node_modules/bootstrap/scss'],
            },
          },
          {
            module: require('@fullhuman/postcss-purgecss'),
            options: {
              content: [
                './app/index.html',
                './app/**.hbs',
                './app/**.js',

                ...emberBootstrapContent(emberBoostrap.whitelist),
              ],
            },
          },
        ],
      },
    },
  });

  ...

};

```

## Contributing

See the [Contributing](CONTRIBUTING.md) guide for details.

## License

This project is licensed under the [MIT License](LICENSE.md).
