'use strict';

const EmberAddon = require('ember-cli/lib/broccoli/ember-addon');
const { emberBootstrapContent } = require('./purgecss');

const emberBoostrap = {
  importBootstrapFont: false,
  bootstrapVersion: 4,
  importBootstrapCSS: false,
  whitelist: ['bs-collapse', 'bs-navbar'],
};

module.exports = function (defaults) {
  const app = new EmberAddon(defaults, {
    'ember-bootstrap': emberBoostrap,
    postcssOptions: {
      compile: {
        enabled: true,
        extension: 'scss',
        parser: require('postcss-scss'),
        plugins: [
          {
            module: require('@csstools/postcss-sass'),
            options: {
              includePaths: ['node_modules/bootstrap/scss'],
            },
          },
          {
            module: require('@fullhuman/postcss-purgecss'),
            options: {
              content: [
                './tests/dummy/app/index.html',
                './tests/dummy/app/templates/application.hbs',
                ...emberBootstrapContent(emberBoostrap.whitelist),
              ],
            },
          },
        ],
      },
    },
  });

  /*
    This build file specifies the options for the dummy test app of this
    addon, located in `/tests/dummy`
    This build file does *not* influence how the addon or the app using it
    behave. You most likely want to be modifying `./index.js` or app's build file
  */

  const { maybeEmbroider } = require('@embroider/test-setup');
  return maybeEmbroider(app, {
    skipBabel: [
      {
        package: 'qunit',
      },
    ],
  });
};
