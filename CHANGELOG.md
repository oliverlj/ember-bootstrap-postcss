# CHANGELOG

<!--- next entry here -->

## 0.1.1
2021-09-22

### Fixes

- wrong path of purgecss files (a64e62fa6019ddf49cf4d41661dae270ca952994)

## 0.1.0
2021-09-18

### Features

- initial version (763e27abc16f274b0a72078fd591d47fa6ca2c3d)

### Fixes

- document addon installation (5c673218c4567f715d6f405d186e91459143a8c9)